# NikoNiko frontend
Just a frontend SPA created with https://github.com/swiip/generator-gulp-angular, using AngularJS, BootStrap...

## Get ready to use it
Just install dependencies
    `npm install && bower install`

## Run it
Run it using Gulp
    `gulp serve`

## Uses backend on localhost:8080
Services are listed with Swagger.
http://localhost:8080/swagger-ui.html