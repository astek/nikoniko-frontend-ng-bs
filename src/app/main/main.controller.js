(function() {
  'use strict';

  angular
    .module('nikonikoFrontendNgBs')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, requestService, $log) {
    var vm = this;

    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.creationDate = 1454500382879;
    vm.showToastr = showToastr;
    vm.votes = [];
    activate();
    vm.days=['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Semaine'];
    vm.weeks=[1,2,3,4,5];
    function activate() {
      getWebDevTec();
      requestService.get('votes').then(function(result) {
         vm.votes = result.plain();
        $log.info(result.plain());
          
      });
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

    function getWebDevTec() {
      vm.awesomeThings = webDevTec.getTec();

      angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }
  }
})();
