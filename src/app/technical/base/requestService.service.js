/* 
 * Copyright (C) 2016.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
angular.module('technical')
        .factory('requestService', requestService);

function requestService($q, Restangular, $http) {

    Restangular.setBaseUrl('http://localhost:8080');

    var returnFactory = {
        "get": get,
        "vote": vote
    };

    function get(url) {
        var deffered = $q.defer();
        Restangular.one(url).get().then(deffered.resolve, deffered.reject);
        return deffered.promise;
    }

    function vote(url) {
        var deffered = $q.defer();
        var body = {};
        url = "http://localhost:8080"+url;
       $http.post(url).then(deffered.resolve, deffered.reject);
//        Restangular.one(url).post('','').then(deffered.resolve, deffered.reject);
        return deffered.promise;
    }

    return returnFactory;
}

